# oVirt-Cockpit SSO

#### Description

Provides `cockpit-ws` service configured to handle SSO from oVirt's Administration Portal to Cockpit running on an oVirt host.

Distributed either as ``rpm`` or ``docker image`` (experimental).

Please note, the provided ``docker image`` is based on the Cockpit-Container project and is so far **experimental only** and work-in-progress.

##### Main use case: 
  - log into oVirt's Administration Portal (available for `admin` users only)
  - find particular host and click `Host Console`
  - host's Cockpit session is opened while no password needs to be entered

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
