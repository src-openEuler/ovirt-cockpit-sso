# oVirt-Cockpit SSO

#### 介绍
oVirt-Cockpit SSO提供了cockpit-ws服务，用于配置管理从oVirt的管理门户到在oVirt主机上Cockpit运行的SSO。

它提供两种方式安装：``rpm`` 或者``docker image``

需要注意的是，``docker image``是基于Cockpit-Container项目，该项目目前正处于实验阶段。
其主要的用例如下：
* 登录到oVirt的管理门户（仅适用于`admin` 用户）
* 找到特定的主机，然后单击`Host Console`
* 主机的Cockpit session是开放的，而无需输入密码

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
