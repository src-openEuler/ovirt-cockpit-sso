Name:           ovirt-cockpit-sso
Version:        0.1.4
Release:        2
Summary:        Provides SSO from oVirt Administration Portal to Cockpit
License:        ASL 2.0
URL:            https://github.com/oVirt/%{name}
Source0:        https://resources.ovirt.org/pub/src/%{name}/%{name}-%{version}.tar.gz

%define build_root_dir %{buildroot}%{_datadir}/%{name}
%define app_root_dir %{_datadir}/%{name}
%define logfile /var/log/ovirt-cockpit-sso.install.log

BuildArch: noarch

# None of the 4.2 features are reuiqred by this package but "Host Console" link is introduced here for the first time
# using conflicts instead of require because in CentOS Virt SIG repo we can't build ovirt-engine and the require
# breaks repository closure
Conflicts: ovirt-engine < 4.4

## TODO: increase to 140 once RHEL 7.5 is released
## In fact, cockpit 140 is required but this is eased to allow smooth
## deployment for testing in the meantime
## cockpit 140 is farther enforced in start.sh which is called by systemd
Requires: cockpit-ws >= 140
Requires: cockpit-dashboard >= 140

Requires: python3

%description
This package sets cockpit-ws service (see cockpit-project.org) to provide
SSO (Single Sign On) from oVirt's Administration Portal to Cockpit running
on an oVirt's host machine.

%prep
%setup -q

%build

%install
mkdir -p %{build_root_dir}/config/cockpit
mkdir -p %{buildroot}%{_usr}/lib/systemd/system/

cp container/config/cockpit/cockpit.conf %{build_root_dir}/config/cockpit/.

cp container/cockpit-auth-ovirt %{build_root_dir}/.
cp container/keygen.sh %{build_root_dir}/.
cp start.sh %{build_root_dir}/.
cp prestart.sh %{build_root_dir}/.
cp ovirt-cockpit-sso.xml %{build_root_dir}/.
cp ovirt-cockpit-sso.service %{buildroot}%{_usr}/lib/systemd/system/.

%post
HOSTNAME=$(hostname -f)
ROOT_DIR=$(echo %{app_root_dir} | sed -e 's/\\/\\\\/g; s/\//\\\//g; s/&/\\\&/g')

case "$1" in
  1)
    echo configuring firewall for ovirt-cockpit-sso service - accept 9986/tcp > %{logfile}
    echo Post-installation configuration of %{name} - setting engine FQDN to: ${HOSTNAME} >> %{logfile}

    ## /bin/firewall-cmd --permanent --zone=public --new-service-from-file=%%{app_root_dir}/ovirt-cockpit-sso.xml
    /bin/firewall-cmd --permanent --add-port 9986/tcp >> %{logfile}
    /bin/firewall-cmd --reload >> %{logfile}
  ;;
  2)
    ## This is an upgrade.
  ;;
esac

/bin/sed -i "s/\%\%ENGINE_URL\%\%/https:\/\/${HOSTNAME}\/ovirt-engine/g" %{app_root_dir}/config/cockpit/cockpit.conf
/bin/sed -i "s/\%\%INSTALL_DIR\%\%/${ROOT_DIR}/g" %{app_root_dir}/config/cockpit/cockpit.conf

%postun
case "$1" in
  0)
    ## package is being removed
    if [[ -e %{app_root_dir}/config/cockpit/ws-certs.d ]]; then
      rm %{app_root_dir}/config/cockpit/ws-certs.d || true
    fi
    if [[ -e %{app_root_dir}/ca.pem ]]; then
      rm %{app_root_dir}/ca.pem || true
    fi

    ## TODO: this is not working but would be better approach:
    ## /bin/firewall-cmd --permanent --zone=public --delete-service=ovirt-cockpit-sso
    /bin/firewall-cmd --permanent --remove-port 9986/tcp >> %{logfile} || true
    /bin/firewall-cmd --reload >> %{logfile}
  ;;
  1)
     ## Package is being upgraded. Do nothing.
    :
  ;;
esac

# the .service file could be changed
systemctl daemon-reload

%files
%doc README.md
%license LICENSE
%{app_root_dir}/config/cockpit/cockpit.conf
%{app_root_dir}/cockpit-auth-ovirt
%{app_root_dir}/keygen.sh
%{app_root_dir}/start.sh
%{app_root_dir}/prestart.sh
%{app_root_dir}/ovirt-cockpit-sso.xml
%{_usr}/lib/systemd/system/ovirt-cockpit-sso.service

%config %verify(not md5 size mtime) %{app_root_dir}/config/cockpit/cockpit.conf

%changelog
* Wed Mar 16 2022 wangdi <wangdi@kylinos.cn> - 0.1.4-2
- fix warning of failing to delete files when uninstall

* Tue Jul 06 2021 weishaokun <weishaokun@kylinos.cn> - 0.1.4
- update package
